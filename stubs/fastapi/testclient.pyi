from fastapi import FastAPI

class Response:
    status_code: int
    def json(self) -> str:
        ...


class TestClient:
    def __init__(self, app: FastAPI):
        ...

    def get(self, url: str) -> Response:
        ...
