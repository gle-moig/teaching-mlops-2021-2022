# Projet MLOPS

*par Guilhem Le Moigne et Victor Leroy*

## Benchmark

### Mémoire

|   |   |
|---|---|
|Taille de l'image|_ Mo|
|RAM utilisée|_ Mo|

### Ping

|   |   |
|---|---|
|min|41 ms|
|avg|56 ms|
|P95|64 ms|
|P99|220 ms|
|worst|363 ms|

### Rapport complet pour 10 utilisateurs

![Rapport Locust](img/locust_report.png "Rapport Locust")

## Consignes

https://tinyurl.com/mlops-paper
