from typing import Optional, Callable, Any

class FastAPI:
    def get(self, url: str, status_code: Optional[int]=200) -> Callable[
        [Callable[..., Any]],
        Callable[..., Any]
    ]:
        ...
