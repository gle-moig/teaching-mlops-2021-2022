from typing import Optional, List, Dict, Generator
from contextlib import contextmanager

class Inference:
    cats: Dict[str, int]

class Model:
    pipe_names: str

    def __call__(self, sentence: Optional[str]) -> Inference:
        ...

    @contextmanager
    def disable_pipes(self, *pipes: str) -> Generator[None, None, None]:
        ...

def load(file_path: str) -> Model:
    ...
