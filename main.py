import fastapi
import spacy
import logging
import json

logging.info("Loading model..")
nlp = spacy.load("./models")

app = fastapi.FastAPI()

@app.get("/api/intent")
def intent_inference(sentence:str = None) -> str:
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    return json.dumps(inference.cats)

@app.get("/api/intent-supported-languages")
def supported_languages() -> str:
    return json.dumps(["fr-FR"])

@app.get("/health", status_code=200)
def health_check_endpoint():
    pass


if __name__ == "__main__":
    import sys
    import uvicorn
    uvicorn.run("main:app", host="0.0.0.0", port=8000, debug=True, reload=True)
