from fastapi.testclient import TestClient
import unittest
import json
import main


class ApiTest(unittest.TestCase):
    CLIENT = TestClient(main.app)

    def test_health(self):
        response = self.CLIENT.get("/health")
        self.assertEqual(response.status_code, 200)

    def test_supported_languages(self):
        response = self.CLIENT.get("/api/intent-supported-languages")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), json.dumps(["fr-FR"]))

    def test_positive_intent_inference(self):
        response = self.CLIENT.get("/api/intent?sentence=")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), json.dumps({
            'purchase': 0.0, 'find-restaurant': 0.0, 'irrelevant': 0.0,
            'find-hotel': 0.0, 'provide-showtimes': 0.0, 'find-around-me': 0.0,
            'find-train': 0.0, 'find-flight': 0.0}))

    def test_negative_intent_inference(self):
        with self.assertRaises(TypeError):
            self.CLIENT.get("/api/intent")

if __name__ == "__main__":
    unittest.main()
